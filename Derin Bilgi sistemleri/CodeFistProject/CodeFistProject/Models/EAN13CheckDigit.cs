﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodeFistProject.Models
{
    public class EAN13CheckDigitAttribute : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string digitKontrol = (string)value;

            if ((digitKontrol.Length) != 13)
            {
                var errorMessage = FormatErrorMessage(validationContext.DisplayName);
                return new ValidationResult(errorMessage);
            }

            else
            {
                return ValidationResult.Success;
            }
        }
    }
}