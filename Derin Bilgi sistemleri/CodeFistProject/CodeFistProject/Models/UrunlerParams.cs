﻿using CodeFistProject.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CodeFistProject.Models
{
    public class UrunlerParams
    {

        public string urunId { get; set; }
        public string Isim { get; set; }

        [EAN13CheckDigit(ErrorMessage= "Barkod EAN13 Formatına uygun değil")]
        public string Barkod { get; set; }

        public Durum Durum { get; set; }

        public decimal Fiyat { get; set; }

        public int KategoriId { get; set; }
        public int Kategori { get; set; }
    }

    public class AllCategories
    {
        public List<Kategori> anaKategoriList { get; set; }
        public List<Kategori> altKategoriList { get; set; }
    }

    public class KategoriList
    {
        public int kategoriID { get; set; }
        public string kategoriAdi { get; set; }
        public int sira { get; set; }
        public int ustKategoriID { get; set; }

    }
}