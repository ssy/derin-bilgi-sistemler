﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodeFistProject.Models
{
    public class KategoriGet
    {
        public int KategoriId { get; set; }        
        public string Isim { get; set; }
        public int Sira { get; set; }
    }
}