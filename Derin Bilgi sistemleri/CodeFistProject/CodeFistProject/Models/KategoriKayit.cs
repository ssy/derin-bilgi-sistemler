﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodeFistProject.Models
{
    public class KategoriKayit
    {
        public int KategoriId { get; set; }
        public string Isim { get; set; }
        public string AltKategoriIsim { get; set; }
        public int Kategori { get; set; }
        public int Sira { get; set; }
        public int UstKategoriId { get; set; }
    }
}