﻿using CodeFistProject.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodeFistProject.Models
{
    public class urunlerModel
    {
        public int UrunId { get; set; }
        public string UrunAdi { get; set; }

        public string Barkod { get; set; }

        public decimal Fiyat { get; set; }

        public int ean13Hesap { get; set; }
        public Durum Durum { get; set; }
    }

    public class urunlerModelViews
    {
        public List<string> UrunAdi { get; set; }

        public List<string> Barkod { get; set; }

        public List<decimal> Fiyat { get; set; }

        public List<int> ean13Hesap { get; set; }
    }
}