﻿using CodeFistProject.Data;
using CodeFistProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodeFistProject.Controllers
{


    public class HomeController : Controller
    {          
        
        public ActionResult Index(string search, int id = 0)
        {
            UrunlerParams editGet = new UrunlerParams();

            // TEXTBOX LARI DOLDUR
            if (id != 0)
            {
                var db = new MvcProjectContext();

                var movie = db.Urun.Find(id);

                editGet.urunId = movie.UrunId.ToString();
                editGet.Isim = movie.Isim;
                editGet.Fiyat = movie.Fiyat;
                editGet.Barkod = movie.Barkod;
                editGet.Durum = movie.Durum;
                editGet.Fiyat = movie.Fiyat;
                editGet.KategoriId = movie.KategoriId;
            }
        
            //TUM KAYITLARI GETİR
            if (search == null)
            {
                ViewBag.categories = getAllCategories();
                ViewBag.EAN13 = EAN13();
            }

            // BARKOD VE ISIME GÖRE ARAMA
            else if (search != null)
            {
                ViewBag.categories = getAllCategories();
                var asd = EAN13();

                ViewBag.EAN13 = asd.Where(x => x.Barkod.Contains(search) || x.UrunAdi.Contains(search)).ToList();
            }

            return View(editGet);
        }

        [HttpPost]
        public ActionResult Index(UrunlerParams kayitParameters, IEnumerable<HttpPostedFileBase> Files)
        {
            var db = new MvcProjectContext();

            try
            {
                // URUN EKLEME
                if (kayitParameters.urunId == null)
                {
                    if (ModelState.IsValid)
                    {
                        db.Urun.Add(new Urun
                        {
                            Isim = kayitParameters.Isim,
                            Barkod = kayitParameters.Barkod,
                            Durum = kayitParameters.Durum,
                            Fiyat = kayitParameters.Fiyat,
                            KategoriId = kayitParameters.Kategori,
                        });

                        db.SaveChanges();

                        ViewBag.categories = getAllCategories();
                        ViewBag.EAN13 = EAN13();
                        urunResimKaydet(Files);
                    }
                }

                // URUN DÜZELTME
                else
                {
                    int urunId = Convert.ToInt16(kayitParameters.urunId);
                    var sdata = db.Urun.Where(x => x.UrunId == urunId).FirstOrDefault();

                    sdata.Isim = kayitParameters.Isim;
                    sdata.Barkod = kayitParameters.Barkod;
                    sdata.Durum = kayitParameters.Durum;
                    sdata.Fiyat = kayitParameters.Fiyat;
                    sdata.KategoriId = kayitParameters.KategoriId;

                    db.SaveChanges();

                    ViewBag.categories = getAllCategories();
                    ViewBag.EAN13 = EAN13();
                }
            }

            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

            return View();
        }

        // KATEGORİ
        private AllCategories getAllCategories()
        {
            var db = new MvcProjectContext();
            AllCategories categories = new AllCategories();

            categories.anaKategoriList = db.Kategori.Where(x => x.UstKategoriId == (int)CategoryType.anaKategori).ToList();
            categories.altKategoriList = db.Kategori.Where(x => x.UstKategoriId != (int)CategoryType.anaKategori).ToList();

            return categories;
        }

        //EAN13 HESABINI YAPMA VE BÜTÜN URUNLERİ EKLEME
        private List<urunlerModel> EAN13()
        {
            List<urunlerModel> Urunler = new List<urunlerModel>();

            var db = new MvcProjectContext();
            var tblUrun = db.Urun.ToList();

            int ciftHaneler = 0;
            int tekHaneler = 0;

            foreach (var item in tblUrun)
            {
                string digitKontrol = item.Barkod;

                for (int i = 0; i < digitKontrol.Length - 1; i++)
                {
                    if (i % 2 == 0)
                    {
                        char dizi = Convert.ToChar(digitKontrol[i]);
                        string parseDigit = Convert.ToString(dizi);
                        ciftHaneler = Convert.ToInt16(parseDigit) + ciftHaneler;
                    }

                    else
                    {
                        char dizi1 = Convert.ToChar(digitKontrol[i]);
                        string parseDigit1 = Convert.ToString(dizi1);
                        tekHaneler = Convert.ToInt16(parseDigit1) + tekHaneler;
                    }
                }

                int tekHaneSonuc = tekHaneler * 3;
                int sum = ciftHaneler + tekHaneSonuc;
                int ean13Hesap = ((1000 - sum) % 10);

                Urunler.Add(new urunlerModel
                {
                    UrunId = item.UrunId,
                    UrunAdi = item.Isim,
                    Barkod = item.Barkod,
                    ean13Hesap = ean13Hesap,
                    Fiyat = item.Fiyat,
                    Durum = item.Durum,
                });
            }
              
            return Urunler;
        }

        public enum CategoryType
        {
            anaKategori = 0
        }

        public HttpPostedFileBase urunResimKaydet(IEnumerable<HttpPostedFileBase> Files)
        {
            var db = new MvcProjectContext();
            var urunId = db.Urun.ToList().Last().UrunId;

            try
            {
                foreach (var file in Files)
                {
                    if (file.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Content/images"), fileName);
                        file.SaveAs(path);

                        db.UrunResmi.Add(new UrunResmi
                        {
                            Path = file.FileName,
                            UrunId = urunId,
                        });
                    }
                }
                db.SaveChanges();
            }

            catch (Exception ex)
            {

            }
            return null;
        }
    
        public ActionResult Delete(int id = 0)
        {
            var db = new MvcProjectContext();

            var urunSil = db.Urun.Where(x => x.UrunId == id).FirstOrDefault();

            db.Urun.Remove(urunSil);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult KategoriEkle()
        {
            ViewBag.categories = getAllCategories();

            return View();
        }
        [HttpPost]
        public ActionResult KategoriEkle(KategoriKayit kategoriKayit)
        {
            var db = new MvcProjectContext();
            string KategoriIsim = "";

            try
            {
                if (kategoriKayit.Isim == null)
                {
                    KategoriIsim = kategoriKayit.AltKategoriIsim;
                }

                if (kategoriKayit.AltKategoriIsim == null)
                {
                    KategoriIsim = kategoriKayit.Isim;
                }

                db.Kategori.Add(new Kategori
                {
                    Isim = KategoriIsim,
                    Sira = kategoriKayit.Sira,
                    UstKategoriId = kategoriKayit.Kategori,
                });

                db.SaveChanges();

                ViewBag.categories = getAllCategories();
            }

            catch (Exception ex)
            {
            }

            return View();
        }
    }
}