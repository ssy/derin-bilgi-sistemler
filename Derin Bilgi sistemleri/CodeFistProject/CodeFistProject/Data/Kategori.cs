﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CodeFistProject.Data
{
    [Table("Kategori")]
    public class Kategori
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int KategoriId { get; set; }
        public string Isim { get; set; }
        public int Sira { get; set; }
        public int UstKategoriId{ get; set; }
        public virtual List<Urun> Urunler { get; set; }
    }
}