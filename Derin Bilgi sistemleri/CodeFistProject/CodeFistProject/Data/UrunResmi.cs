﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CodeFistProject.Data
{
    [Table("UrunResmi")]
    public class UrunResmi
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int UrunResmiId { get; set; }
        public string Path { get; set; }
        public int UrunId { get; set; }
        public virtual List<Urun> Urunler { get; set; }
    }
}