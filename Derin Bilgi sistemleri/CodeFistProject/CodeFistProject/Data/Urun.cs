﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CodeFistProject.Data
{
    public enum Durum
    {
        YayindanKaldirilmis = 0,
        Yayinda = 1,
    }

    [Table("Urun")]
    public class Urun
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int UrunId { get; set; }
        public string Isim { get; set; }
        public string Barkod { get; set; }
        public Durum Durum { get; set; }
        public decimal Fiyat { get; set; }
        public int KategoriId { get; set; }
        public virtual List<Kategori> Kategoriler { get; set; }
    }
}